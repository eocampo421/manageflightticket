package ar.com.dataart.dao;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static java.sql.Date.valueOf;
import static java.sql.Time.valueOf;

@Repository
public class FlightTicketImplDao implements FlightTicketDao {

    private static final String INSERT_INTO_FLIGHT_TICKET = "INSERT INTO FLIGHT_TICKET (departureDate, arrivalDate, cityOfOrigin, destinationCity," +
            "passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SELECT_TICKET_ID = "SELECT * FROM FLIGHT_TICKET WHERE ticketID = ?";

    private JdbcTemplate jdbcTemplate;
    private KeyHolderFactory keyHolderFactory;

    @Autowired
    public FlightTicketImplDao(JdbcTemplate jdbcTemplate, KeyHolderFactory keyHolderFactory) {
        this.jdbcTemplate = jdbcTemplate;
        this.keyHolderFactory = keyHolderFactory;
    }

    @Override
    public Long insertFlightTicket(FlightTicket flightTicket) {
        KeyHolder keyHolder = keyHolderFactory.newKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection
                    .prepareStatement(INSERT_INTO_FLIGHT_TICKET, Statement.RETURN_GENERATED_KEYS);

            ps.setDate(1, valueOf(flightTicket.getDepartureDate()));
            ps.setDate(2, valueOf(flightTicket.getArrivalDate()));
            ps.setString(3, flightTicket.getCityOfOrigin());
            ps.setString(4, flightTicket.getDestinationCity());
            ps.setString(5, flightTicket.getPassengerName());
            ps.setInt(6, flightTicket.getPassengerAge());
            ps.setBoolean(7, flightTicket.isHasBaggageDeposit());
            ps.setDouble(8, flightTicket.getPrice());
            ps.setTime(9, valueOf(flightTicket.getDepartureTime()));
            ps.setTime(10, valueOf(flightTicket.getArrivalTime()));

            return ps;
        }, keyHolder);

        Long newUserId = keyHolder.getKey().longValue();
        return newUserId;
    }


    @Override
    public FlightTicket getTicketItinerary(Long ticketItineraryID) {
        FlightTicketMapper flightTicketMapper = new FlightTicketMapper();
        FlightTicket flightTicket;
        try {
            flightTicket = jdbcTemplate.queryForObject(SELECT_TICKET_ID, flightTicketMapper, ticketItineraryID);
        } catch (EmptyResultDataAccessException e) {
            throw new TechnicalException(String.format("[ERROR] The ticket itinerary ID %d does not exist.", ticketItineraryID), e);
        }

        return flightTicket;
    }

    class FlightTicketMapper implements RowMapper<FlightTicket> {

        @Override
        public FlightTicket mapRow(ResultSet rs, int i) {
            FlightTicket flightTicket = new FlightTicket();
            try {
                flightTicket.setDepartureDate(rs.getDate("departureDate").toLocalDate());
                flightTicket.setArrivalDate(rs.getDate("arrivalDate").toLocalDate());
                flightTicket.setCityOfOrigin(rs.getString("cityOfOrigin"));
                flightTicket.setDestinationCity(rs.getString("destinationCity"));
                flightTicket.setPassengerName(rs.getString("passengerName"));
                flightTicket.setPassengerAge(rs.getInt("passengerAge"));
                flightTicket.setHasBaggageDeposit(rs.getBoolean("isHasBaggageDeposit"));
                flightTicket.setPrice(rs.getDouble("price"));
                flightTicket.setDepartureTime(rs.getTime("departureTime").toLocalTime());
                flightTicket.setArrivalTime(rs.getTime("arrivalTime").toLocalTime());
            } catch (SQLException e) {
                throw new TechnicalException(String.format("[ERROR] An error has occurred in retrieving a record in the database."), e);
            }

            return flightTicket;
        }
    }
}
