package ar.com.dataart.dao;

import ar.com.dataart.bo.entities.FlightTicket;

interface FlightTicketDao {

    Long insertFlightTicket(FlightTicket flightTicket);

    FlightTicket getTicketItinerary(Long ticketItineraryID);
}
