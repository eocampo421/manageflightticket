package ar.com.dataart.service;

import ar.com.dataart.adapter.FlightTicketRequestAdapter;
import ar.com.dataart.adapter.FlightTicketResponseAdapter;
import ar.com.dataart.bo.FlightTicketBO;
import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.BusinessException;
import ar.com.dataart.bo.exception.TechnicalException;
import ar.com.dataart.http.ErrorResponse;
import ar.com.dataart.http.FlightTicketRequest;
import ar.com.dataart.http.FlightTicketResponse;
import ar.com.dataart.http.TicketItineraryResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ManageFlightTicketService {
    private static final Logger LOG = LoggerFactory.getLogger(ManageFlightTicketService.class);

    private final FlightTicketBO flightTicketBO;

    @Autowired
    public ManageFlightTicketService(FlightTicketBO flightTicketBO) {
        this.flightTicketBO = flightTicketBO;
    }

    @ApiOperation(value = "Creates a flight itinerary ID",
            response = TicketItineraryResponse.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successful ticket creation"),
            @ApiResponse(code = 400, message = "Wrong parameter"),
            @ApiResponse(code = 500, message = "Unknown error, e.g. database connection") })
    @PostMapping(value = "/flightTickets")
    public ResponseEntity<Object> createFlightItineraryID(@RequestBody FlightTicketRequest request) {
        ResponseEntity<Object> responseEntity;

        try {
            FlightTicket flightTicket = new FlightTicketRequestAdapter().apply(request);

            Long ticketItineraryID = flightTicketBO.createTicketItineraryID(flightTicket);

            TicketItineraryResponse ticketItineraryResponse = new TicketItineraryResponse();
            ticketItineraryResponse.setFlightTicketID(ticketItineraryID);
            responseEntity = new ResponseEntity<>(ticketItineraryResponse, CREATED);

            LOG.info("The flight ticket ID {} was created", ticketItineraryID);
        } catch (BusinessException e) {
            LOG.info("Business error", e.getMessage());
            responseEntity = createBusinessExceptionResponse(e);
        } catch (TechnicalException e) {
            LOG.error("Technical error", e);
            ErrorResponse technicalErrorResponse = new ErrorResponse(e.getMessage(), INTERNAL_SERVER_ERROR.toString());
            responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body(technicalErrorResponse);
        } catch (Exception e) {
            LOG.error("Unknown error");
            ErrorResponse unknownErrorResponse = new ErrorResponse(e.getMessage(), INTERNAL_SERVER_ERROR.toString());
            responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body(unknownErrorResponse);
        }

        return responseEntity;
    }

    @ApiOperation(value = "Gets the flight ticket to an identifier",
            response = FlightTicketResponse.class)
    @ApiResponses(value = { @ApiResponse(code = 302, message = "Identifier was found."),
            @ApiResponse(code = 400, message = "Wrong parameter"),
            @ApiResponse(code = 500, message = "Unknown error, e.g. database connection") })
    @GetMapping(value = "/flightTickets/{ID}")
    public ResponseEntity<Object> getFlightTicket(@PathVariable("ID") Long ticketItineraryID) {
        ResponseEntity<Object> responseEntity;

        try {
            FlightTicket flightTicket = flightTicketBO.getTicketItineraryID(ticketItineraryID);

            FlightTicketResponseAdapter flightTicketResponseAdapter = new FlightTicketResponseAdapter(ticketItineraryID);
            FlightTicketResponse flightTicketResponse = flightTicketResponseAdapter.apply(flightTicket);

            LOG.info("The flight ticket ID {} was founded", ticketItineraryID);
            responseEntity = new ResponseEntity<>(flightTicketResponse, HttpStatus.FOUND);
        } catch (BusinessException e) {
            LOG.info("Business error", e.getMessage());
            responseEntity = createBusinessExceptionResponse(e);
        } catch (TechnicalException e) {
            LOG.error("Technical error", e);
            ErrorResponse technicalErrorResponse = new ErrorResponse(e.getMessage(), INTERNAL_SERVER_ERROR.toString());
            responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body(technicalErrorResponse);
        } catch (Exception e) {
            LOG.error("Unknown error", e);
            ErrorResponse unknownErrorResponse = new ErrorResponse(e.getMessage(), INTERNAL_SERVER_ERROR.toString());
            responseEntity = ResponseEntity.status(INTERNAL_SERVER_ERROR).body(unknownErrorResponse);
        }

        return responseEntity;
    }

    private ResponseEntity<Object> createBusinessExceptionResponse(BusinessException businessException) {
        ResponseEntity<Object> response;

        switch (businessException.getBusinessRuleError()) {
            case NULL_PARAM:
            case BAD_FORMAT:
                response = ResponseEntity.badRequest().body(new ErrorResponse(businessException.getMessage(), HttpStatus.BAD_REQUEST.toString()));
                break;
            default:
                response = ResponseEntity.status(NOT_FOUND).body(new ErrorResponse(businessException.getMessage(), NOT_FOUND.toString()));
        }

        return response;
    }
}
