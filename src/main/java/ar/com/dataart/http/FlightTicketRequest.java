package ar.com.dataart.http;

import io.swagger.annotations.ApiModelProperty;

public class FlightTicketRequest {

    @ApiModelProperty(notes = "Departure's date", required = true, example = "20/05/12")
    private String departureDate;
    @ApiModelProperty(notes = "Arrival's date", required = true, example = "20/05/12")
    private String arrivalDate;
    @ApiModelProperty(notes = "City of origin", required = true, example = "Miami")
    private String cityOfOrigin;
    @ApiModelProperty(notes = "Destination city", required = true, example = "Mexico")
    private String destinationCity;
    @ApiModelProperty(notes = "Passenger's name", required = true, example = "John")
    private String passengerName;
    @ApiModelProperty(notes = "Passenger's age", required = true, example = "35")
    private Integer passengerAge;
    @ApiModelProperty(notes = "If you have baggage in the deposit.", required = true, example = "false")
    private boolean isHasBaggageDeposit;
    @ApiModelProperty(notes = "Ticket's price", required = true, example = "500.50")
    private Double price;
    @ApiModelProperty(notes = "Departure's time", required = true, example = "10:50")
    private String departureTime;
    @ApiModelProperty(notes = "Arrival's time", required = true, example = "22:50")
    private String arrivalTime;

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getCityOfOrigin() {
        return cityOfOrigin;
    }

    public void setCityOfOrigin(String cityOfOrigin) {
        this.cityOfOrigin = cityOfOrigin;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public Integer getPassengerAge() {
        return passengerAge;
    }

    public void setPassengerAge(Integer passengerAge) {
        this.passengerAge = passengerAge;
    }

    public boolean isHasBaggageDeposit() {
        return isHasBaggageDeposit;
    }

    public void setHasBaggageDeposit(boolean hasBaggageDeposit) {
        isHasBaggageDeposit = hasBaggageDeposit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
