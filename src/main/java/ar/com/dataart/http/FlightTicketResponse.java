package ar.com.dataart.http;

import io.swagger.annotations.ApiModelProperty;

public class FlightTicketResponse {

    @ApiModelProperty(notes = "Flight itinerary ID")
    private Long flightTicketID;
    @ApiModelProperty(notes = "Departure's date")
    private String departureDate;
    @ApiModelProperty(notes = "Arrival date")
    private String arrivalDate;
    @ApiModelProperty(notes = "City of origin")
    private String cityOfOrigin;
    @ApiModelProperty(notes = "Destination city")
    private String destinationCity;
    @ApiModelProperty(notes = "Passenger's name")
    private String passengerName;
    @ApiModelProperty(notes = "Passenger's age")
    private int passengerAge;
    @ApiModelProperty(notes = "If you have baggage in the deposit.")
    private boolean isHasBaggageDeposit;
    @ApiModelProperty(notes = "Ticket's price")
    private double price;
    @ApiModelProperty(notes = "Departure's time")
    private String departureTime;
    @ApiModelProperty(notes = "Arrival's time")
    private String arrivalTime;

    public Long getFlightTicketID() {
        return flightTicketID;
    }

    public void setFlightTicketID(Long flightTicketID) {
        this.flightTicketID = flightTicketID;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getCityOfOrigin() {
        return cityOfOrigin;
    }

    public void setCityOfOrigin(String cityOfOrigin) {
        this.cityOfOrigin = cityOfOrigin;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public int getPassengerAge() {
        return passengerAge;
    }

    public void setPassengerAge(int passengerAge) {
        this.passengerAge = passengerAge;
    }

    public boolean isHasBaggageDeposit() {
        return isHasBaggageDeposit;
    }

    public void setHasBaggageDeposit(boolean hasBaggageDeposit) {
        isHasBaggageDeposit = hasBaggageDeposit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public String toString() {
        return "FlightTicketResponse{" +
                "departureDate=" + departureDate +
                ", arrivalDate=" + arrivalDate +
                ", cityOfOrigin='" + cityOfOrigin + '\'' +
                ", destinationCity='" + destinationCity + '\'' +
                ", passengerName='" + passengerName + '\'' +
                ", passengerAge=" + passengerAge +
                ", isHasBaggageDeposit=" + isHasBaggageDeposit +
                ", price=" + price +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                '}';
    }
}
