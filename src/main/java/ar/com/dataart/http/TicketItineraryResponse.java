package ar.com.dataart.http;

import io.swagger.annotations.ApiModelProperty;

public class TicketItineraryResponse {

    @ApiModelProperty(notes = "Flight itinerary ID", required = true, example = "5")
    private Long flightTicketID;

    public Long getFlightTicketID() {
        return flightTicketID;
    }

    public void setFlightTicketID(Long flightTicketID) {
        this.flightTicketID = flightTicketID;
    }
}
