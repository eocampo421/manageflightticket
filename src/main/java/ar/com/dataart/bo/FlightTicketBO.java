package ar.com.dataart.bo;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.BusinessException;
import ar.com.dataart.dao.FlightTicketImplDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static ar.com.dataart.bo.entities.BusinessErrorRule.NULL_PARAM;
import static ar.com.dataart.utils.Assert.notNull;

@Service
public class FlightTicketBO {

    private FlightTicketImplDao flightTicketImplDao;

    @Autowired
    public FlightTicketBO(FlightTicketImplDao flightTicketImplDao) {
        this.flightTicketImplDao = flightTicketImplDao;
    }

    public Long createTicketItineraryID(FlightTicket flightTicket) {
        validateFlightTicketParameter(flightTicket);
        Long ticketItinerary = flightTicketImplDao.insertFlightTicket(flightTicket);

        return ticketItinerary;
    }

    public FlightTicket getTicketItineraryID(Long itineraryID) {
        FlightTicket flightTicket;
        try {
            notNull(itineraryID, "itineraryID");
            flightTicket = flightTicketImplDao.getTicketItinerary(itineraryID);
        }catch (IllegalArgumentException e){
            throw new BusinessException(NULL_PARAM, e.getMessage());
        }

        return flightTicket;
    }

    private void validateFlightTicketParameter(FlightTicket flightTicket) {
        try {
            notNull(flightTicket.getCityOfOrigin(), "cityOfOrigin");
            notNull(flightTicket.getDestinationCity(), "destinationCity");
            notNull(flightTicket.getPassengerName(), "passengerName");
            notNull(flightTicket.getPassengerAge(), "passengerAge");
            notNull(flightTicket.isHasBaggageDeposit(), "isHasBaggageDeposit");
            notNull(flightTicket.getPrice(), "price");
        } catch (IllegalArgumentException e) {
            throw new BusinessException(NULL_PARAM, e.getMessage());
        }
    }


}
