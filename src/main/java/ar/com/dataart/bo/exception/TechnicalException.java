package ar.com.dataart.bo.exception;

public class TechnicalException extends RuntimeException {

    private static final long serialVersionUID = -6299218868523262974L;

    public TechnicalException() {
        super();
    }

    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicalException(String code, String msg) {
        super(msg);
    }
}