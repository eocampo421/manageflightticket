package ar.com.dataart.bo.exception;

public class NullArgumentException extends IllegalArgumentException {

    public NullArgumentException(String parameterName) {
        super(String.format("'%s' cannot be null.", parameterName));
    }
}
