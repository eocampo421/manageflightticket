package ar.com.dataart.bo.exception;


import ar.com.dataart.bo.entities.BusinessErrorRule;

public class BusinessException extends RuntimeException {

    private final BusinessErrorRule businessRuleError;

    public BusinessException(BusinessErrorRule businessRuleError, String message) {
        super(message);
        this.businessRuleError = businessRuleError;
    }

    public BusinessErrorRule getBusinessRuleError() {
        return businessRuleError;
    }
}
