package ar.com.dataart.bo.entities;

import java.time.LocalDate;
import java.time.LocalTime;

public class FlightTicket {

    private LocalDate departureDate;
    private LocalDate arrivalDate;
    private String cityOfOrigin;
    private String destinationCity;
    private String passengerName;
    private Integer passengerAge;
    private boolean isHasBaggageDeposit;
    private Double price;
    private LocalTime departureTime;
    private LocalTime arrivalTime;

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getCityOfOrigin() {
        return cityOfOrigin;
    }

    public void setCityOfOrigin(String cityOfOrigin) {
        this.cityOfOrigin = cityOfOrigin;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public Integer getPassengerAge() {
        return passengerAge;
    }

    public void setPassengerAge(Integer passengerAge) {
        this.passengerAge = passengerAge;
    }

    public boolean isHasBaggageDeposit() {
        return isHasBaggageDeposit;
    }

    public void setHasBaggageDeposit(boolean hasBaggageDeposit) {
        isHasBaggageDeposit = hasBaggageDeposit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}