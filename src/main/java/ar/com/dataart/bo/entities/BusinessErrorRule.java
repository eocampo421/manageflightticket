package ar.com.dataart.bo.entities;

public enum BusinessErrorRule {
    NULL_PARAM("NULL_PARAM"),
    BAD_FORMAT("BAD_FORMAT");

    private String businessError;


    private BusinessErrorRule(String businessError) {
        this.businessError = businessError;
    }
}
