package ar.com.dataart.utils;

import ar.com.dataart.bo.exception.TechnicalException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateFormatter {

    private static final String INVALID_DATE = "The value %s is not a valid date.";

    static final DateTimeFormatter FORMAT_WITH_HOUR = DateTimeFormatter.ofPattern("HH:mm");
    static final DateTimeFormatter FORMAT_ONLY_DATE = DateTimeFormatter.ofPattern("yy/MM/dd");

    private DateFormatter() {
    }

    public static LocalTime toTime(String date) {
        return timeParse(FORMAT_WITH_HOUR, date);
    }

    public static LocalDate toDate(String date) {
        return dateParse(FORMAT_ONLY_DATE, date);
    }

    public static String fromTime(LocalTime date) {
        return FORMAT_WITH_HOUR.format(date);
    }

    public static String fromDate(LocalDate date) {
        return FORMAT_ONLY_DATE.format(date);
    }

    private static LocalDate dateParse(DateTimeFormatter dateFormatter, String date) {
        try {
            return LocalDate.parse(date, dateFormatter);
        } catch (DateTimeParseException e) {
            throw new TechnicalException(String.format(INVALID_DATE, date), e);
        }
    }

    private static LocalTime timeParse(DateTimeFormatter dateFormatter, String date) {
        try {
            return LocalTime.parse(date, dateFormatter);
        } catch (DateTimeParseException e) {
            throw new TechnicalException(String.format(INVALID_DATE, date), e);
        }
    }
}
