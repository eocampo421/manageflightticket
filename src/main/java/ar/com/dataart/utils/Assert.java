package ar.com.dataart.utils;

import ar.com.dataart.bo.exception.NullArgumentException;

public class Assert {

    private Assert() {
    }

    public static <T> T notNull(T value, String parameterName) {
        if (value != null) {
            return value;
        } else {
            throw new NullArgumentException(parameterName);
        }
    }
}
