package ar.com.dataart.adapter;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.BusinessException;
import ar.com.dataart.bo.exception.NullArgumentException;
import ar.com.dataart.http.FlightTicketRequest;

import java.util.function.Function;

import static ar.com.dataart.bo.entities.BusinessErrorRule.NULL_PARAM;
import static ar.com.dataart.utils.Assert.notNull;
import static ar.com.dataart.utils.DateFormatter.toDate;
import static ar.com.dataart.utils.DateFormatter.toTime;

public class FlightTicketRequestAdapter implements Function<FlightTicketRequest, FlightTicket> {

    @Override
    public FlightTicket apply(FlightTicketRequest request) {
        FlightTicket flightTicket = new FlightTicket();

        try {
            createDateTimeFlightTicket(request, flightTicket);
        } catch (NullArgumentException e) {
            throw new BusinessException(NULL_PARAM, e.getMessage());
        }

        flightTicket.setCityOfOrigin(request.getCityOfOrigin());
        flightTicket.setDestinationCity(request.getDestinationCity());
        flightTicket.setPassengerName(request.getPassengerName());
        flightTicket.setPassengerAge(request.getPassengerAge());
        flightTicket.setHasBaggageDeposit(request.isHasBaggageDeposit());
        flightTicket.setPrice(request.getPrice());

        return flightTicket;
    }

    private void createDateTimeFlightTicket(FlightTicketRequest request, FlightTicket flightTicket) {
        flightTicket.setDepartureDate(toDate(notNull(request.getDepartureDate(), "departureDate")));
        flightTicket.setArrivalDate(toDate(notNull(request.getArrivalDate(), "arrivalDate")));
        flightTicket.setDepartureTime(toTime(notNull(request.getDepartureTime(), "departureTime")));
        flightTicket.setArrivalTime(toTime(notNull(request.getArrivalTime(), "arrivalTime")));
    }
}
