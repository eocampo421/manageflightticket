package ar.com.dataart.adapter;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.http.FlightTicketResponse;

import java.util.function.Function;

import static ar.com.dataart.utils.DateFormatter.fromDate;
import static ar.com.dataart.utils.DateFormatter.fromTime;

public class FlightTicketResponseAdapter implements Function<FlightTicket, FlightTicketResponse> {

    private final Long ticketItineraryID;

    public FlightTicketResponseAdapter(Long ticketItineraryID) {
        this.ticketItineraryID = ticketItineraryID;
    }

    @Override
    public FlightTicketResponse apply(FlightTicket flightTicket) {
        FlightTicketResponse flightTicketResponse = new FlightTicketResponse();

        flightTicketResponse.setFlightTicketID(ticketItineraryID);
        flightTicketResponse.setDepartureDate(fromDate(flightTicket.getDepartureDate()));
        flightTicketResponse.setArrivalDate(fromDate(flightTicket.getArrivalDate()));
        flightTicketResponse.setCityOfOrigin(flightTicket.getCityOfOrigin());
        flightTicketResponse.setDestinationCity(flightTicket.getDestinationCity());
        flightTicketResponse.setPassengerName(flightTicket.getPassengerName());
        flightTicketResponse.setPassengerAge(flightTicket.getPassengerAge());
        flightTicketResponse.setPrice(flightTicket.getPrice());
        flightTicketResponse.setHasBaggageDeposit(flightTicket.isHasBaggageDeposit());
        flightTicketResponse.setDepartureTime(fromTime(flightTicket.getDepartureTime()));
        flightTicketResponse.setArrivalTime(fromTime(flightTicket.getArrivalTime()));

        return flightTicketResponse;
    }
}
