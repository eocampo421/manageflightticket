DROP TABLE IF EXISTS flight_ticket;

CREATE TABLE flight_ticket (ticketID BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
 departureDate DATE,
 arrivalDate DATE,
 cityOfOrigin VARCHAR (100),
 destinationCity VARCHAR (100),
 passengerName VARCHAR (20),
 passengerAge INTEGER,
 isHasBaggageDeposit BOOLEAN,
 price DECIMAL,
 departureTime TIME,
 arrivalTime TIME);

CREATE UNIQUE INDEX idx_ue on flight_ticket(ticketID, departureDate, arrivalDate, cityOfOrigin, destinationCity);

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2020-04-08', '2020-04-08', 'New York', 'Portland', 'John', 25, TRUE, 300.00, '01:00:00', '03:00:00');

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2020-04-14', '2020-04-14', 'New York', 'Miami', 'Juan', 30, TRUE, 500.00, '22:00:00', '02:00:00');

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2020-04-23', '2020-04-23', 'England', 'Italy', 'Mike', 45, TRUE, 252.50, '10:50:00', '15:00:00');

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2009-02-04', '2009-02-04', 'Mexico', 'La Vegas', 'Charles', 20, TRUE, 332.00, '01:00:00', '06:00:00');

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2009-05-04', '2009-05-04', 'Cordoba', 'Buenos Aires', 'Peter', 22, TRUE, 100.00, '15:00:00', '16:00:00');

INSERT INTO flight_ticket (departureDate, arrivalDate, cityOfOrigin, destinationCity,
                          passengerName, passengerAge, isHasBaggageDeposit, price, departureTime, arrivalTime)
VALUES ('2009-07-04', '2009-07-04', 'Cordoba', 'Bariloche', 'Elena', 18, TRUE, 2000.00, '20:00:00', '22:30:00');

--SELECT * FROM flight_ticket