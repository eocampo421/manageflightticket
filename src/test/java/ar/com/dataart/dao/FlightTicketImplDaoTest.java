package ar.com.dataart.dao;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.TechnicalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.time.LocalTime;

import static java.time.LocalDate.of;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FlightTicketImplDaoTest {

    private FlightTicketImplDao flightTicketImplDao;

    @Mock
    private JdbcTemplate jdbcTemplate;
    @Mock
    private KeyHolderFactory keyHolderFactory;
    @Mock
    private GeneratedKeyHolder generatedKeyHolder;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        flightTicketImplDao = new FlightTicketImplDao(jdbcTemplate, keyHolderFactory);
    }

    @Test
    public void when_flightTicket_isInserted_then_generate_ticketItineraryId() {

        KeyHolder keyHolder = generatedKeyHolder;
        when(keyHolderFactory.newKeyHolder()).thenReturn(keyHolder);
        when(generatedKeyHolder.getKey()).thenReturn(5L);
        when(jdbcTemplate.update(anyString(), any(PreparedStatementCreator.class), any(KeyHolder.class))).thenReturn(1);

        Long ticketItineraryID = flightTicketImplDao.insertFlightTicket(new FlightTicket());

        assertEquals(new Long(5L), ticketItineraryID);
    }

    @Test
    public void when_ticketItineraryID_exist_then_return_flight_ticket() {

        FlightTicket flightTicket = prepareFlightTicket();

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class), anyLong())).thenReturn(flightTicket);

        FlightTicket foundedFlightTicket = flightTicketImplDao.getTicketItinerary(new Long(2));

        assertEquals(of(2020, 10, 25), foundedFlightTicket.getDepartureDate());
        assertEquals(of(2020, 10, 25), foundedFlightTicket.getArrivalDate());
        assertEquals("New York", foundedFlightTicket.getCityOfOrigin());
        assertEquals("Italy", foundedFlightTicket.getDestinationCity());
        assertEquals("John", foundedFlightTicket.getPassengerName());
        assertEquals(new Integer(23), foundedFlightTicket.getPassengerAge());
        assertEquals(new Double(500.50), foundedFlightTicket.getPrice());
        assertEquals(true, foundedFlightTicket.isHasBaggageDeposit());
        assertEquals(LocalTime.of(10, 50), foundedFlightTicket.getDepartureTime());
        assertEquals(LocalTime.of(22, 50), foundedFlightTicket.getArrivalTime());
    }

    private FlightTicket prepareFlightTicket() {
        FlightTicket flightTicket = new FlightTicket();
        flightTicket.setDepartureDate(of(2020, 10, 25));
        flightTicket.setArrivalDate(of(2020, 10, 25));
        flightTicket.setCityOfOrigin("New York");
        flightTicket.setDestinationCity("Italy");
        flightTicket.setPassengerName("John");
        flightTicket.setPassengerAge(23);
        flightTicket.setPrice(500.50);
        flightTicket.setHasBaggageDeposit(true);
        flightTicket.setDepartureTime(LocalTime.of(10, 50));
        flightTicket.setArrivalTime(LocalTime.of(22, 50));

        return flightTicket;
    }

    @Test
    public void when_ticketItineraryID_doesNot_exist_then_throw_technical_exception() {

        expectedException.expect(TechnicalException.class);
        expectedException.expectMessage(is("[ERROR] The ticket itinerary ID 3 does not exist."));

        doThrow(EmptyResultDataAccessException.class).when(jdbcTemplate).queryForObject(anyString(), any(RowMapper.class), anyLong());

        FlightTicket flightTicket = flightTicketImplDao.getTicketItinerary(3L);

        verify(jdbcTemplate, times(1)).queryForObject(anyString(), any(RowMapper.class), anyLong());
    }
}
