package ar.com.dataart.bo;

import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.BusinessException;
import ar.com.dataart.dao.FlightTicketImplDao;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class FlightTicketBOTest {

    private FlightTicketBO flightTicketBO;

    @Mock
    private FlightTicketImplDao flightTicketImplDao;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        flightTicketBO = new FlightTicketBO(flightTicketImplDao);
    }

    @Test
    public void when_flightTicket_isInserted_then_return_ticketItineraryId() {
        FlightTicket flightTicket = prepareFlightTicket();

        when(flightTicketImplDao.insertFlightTicket(any(FlightTicket.class))).thenReturn(4L);

        Long createdTicketItineraryId = flightTicketBO.createTicketItineraryID(flightTicket);

        assertEquals(new Long(4L), createdTicketItineraryId);
    }

    @Test
    public void when_flightTicket_isIncomplete_then_throw_businessException() {
        expectedException.expect(BusinessException.class);
        expectedException.expectMessage(is("'passengerName' cannot be null."));

        FlightTicket flightTicket = new FlightTicket();
        flightTicket.setCityOfOrigin("New York");
        flightTicket.setDestinationCity("Italy");
        flightTicket.setPassengerAge(23);

        Long createdTicketItineraryId = flightTicketBO.createTicketItineraryID(flightTicket);

        verify(flightTicketImplDao, times(0)).insertFlightTicket(any(FlightTicket.class));
    }

    @Test
    public void when_ticketItineraryId_is_null_then_throw_businessException() {
        expectedException.expect(BusinessException.class);
        expectedException.expectMessage(is("'itineraryID' cannot be null."));

        Long ticketItineraryId = null;

        FlightTicket flightTicket = flightTicketBO.getTicketItineraryID(ticketItineraryId);

        verify(flightTicketImplDao, times(0)).getTicketItinerary(anyLong());
    }

    @Test
    public void when_ticketItineraryId_isFounded_then_return_flightTicket() {

        when(flightTicketImplDao.getTicketItinerary(anyLong())).thenReturn(new FlightTicket());

        Long ticketItineraryID = new Long(4);
        FlightTicket flightTicket = flightTicketBO.getTicketItineraryID(ticketItineraryID);

        assertNotNull(flightTicket);
    }


    private FlightTicket prepareFlightTicket() {
        FlightTicket flightTicket = new FlightTicket();

        flightTicket.setCityOfOrigin("New York");
        flightTicket.setDestinationCity("Italy");
        flightTicket.setPassengerName("John");
        flightTicket.setPassengerAge(23);
        flightTicket.setPrice(500.50);
        flightTicket.setHasBaggageDeposit(true);

        return flightTicket;
    }
}