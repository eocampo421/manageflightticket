package ar.com.dataart.service;

import ar.com.dataart.bo.FlightTicketBO;
import ar.com.dataart.bo.entities.FlightTicket;
import ar.com.dataart.bo.exception.TechnicalException;
import ar.com.dataart.http.FlightTicketRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.time.LocalTime;

import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class ManageFlightTicketServiceTest {

    private static final Long TICKET_ITINERARY_ID = 4L;

    private ManageFlightTicketService manageFlightTicketService;
    private FlightTicketRequest flightTicketRequest;
    private FlightTicket flightTicket;

    @Mock
    private FlightTicketBO flightTicketBO;

    @Before
    public void setUp() {
        manageFlightTicketService = new ManageFlightTicketService(flightTicketBO);
        flightTicketRequest = new FlightTicketRequest();
        flightTicket = new FlightTicket();
    }

    @Test
    public void when_flightTicket_isCreated_then_response_isOK() {
        FlightTicketRequest flightTicketRequest = createFlightTicketRequest();

        when(flightTicketBO.createTicketItineraryID(any(FlightTicket.class))).thenReturn(TICKET_ITINERARY_ID);

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.createFlightItineraryID(flightTicketRequest);

        assertNotNull(flightTicketResponseEntity.getBody());
        assertEquals(CREATED, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_flightTicket_has_nullDate_then_response_badRequest() {
        flightTicketRequest.setDepartureDate(null);

        when(flightTicketBO.createTicketItineraryID(any(FlightTicket.class))).thenReturn(TICKET_ITINERARY_ID);

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.createFlightItineraryID(flightTicketRequest);

        assertNotNull(flightTicketResponseEntity.getBody());
        assertEquals(BAD_REQUEST, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_flightTicket_has_bad_formatDate_then_response_internalServerError() {
        flightTicketRequest.setDepartureDate("10/02/2020");

        when(flightTicketBO.createTicketItineraryID(any(FlightTicket.class))).thenReturn(TICKET_ITINERARY_ID);

        ResponseEntity<Object> flightTicketResponseResponseEntity = manageFlightTicketService.createFlightItineraryID(flightTicketRequest);

        assertNotNull(flightTicketResponseResponseEntity.getBody());
        assertEquals(INTERNAL_SERVER_ERROR, flightTicketResponseResponseEntity.getStatusCode());
    }

    @Test
    public void when_technical_error_occurred_then_response_internal_server_error() {
        flightTicketRequest = createFlightTicketRequest();

        doThrow(TechnicalException.class).when(flightTicketBO).createTicketItineraryID(any(FlightTicket.class));

        ResponseEntity<Object> flightTicketResponseResponseEntity = manageFlightTicketService.createFlightItineraryID(flightTicketRequest);

        assertEquals(INTERNAL_SERVER_ERROR, flightTicketResponseResponseEntity.getStatusCode());
    }

    @Test
    public void when_unexpected_error_occurred_then_response_internal_server_error() {
        flightTicketRequest = createFlightTicketRequest();

        doThrow(RuntimeException.class).when(flightTicketBO).createTicketItineraryID(any(FlightTicket.class));

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.createFlightItineraryID(flightTicketRequest);

        assertEquals(INTERNAL_SERVER_ERROR, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_getTicketItinerary_exits_then_response_found() {
        flightTicket = createFlightTicket();

        when(flightTicketBO.getTicketItineraryID(anyLong())).thenReturn(flightTicket);

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.getFlightTicket(TICKET_ITINERARY_ID);

        assertNotNull(flightTicketResponseEntity.getBody());
        assertEquals(FOUND, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_getTicketItinerary_isNull_then_response_bad_request() {
        when(flightTicketBO.getTicketItineraryID((Long) isNull())).thenCallRealMethod();

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.getFlightTicket(null);

        assertEquals(BAD_REQUEST, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_getTicketItinerary_throw_technicalException_then_response_internalServerError() {
        doThrow(TechnicalException.class).when(flightTicketBO).getTicketItineraryID(anyLong());

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.getFlightTicket(TICKET_ITINERARY_ID);

        assertEquals(INTERNAL_SERVER_ERROR, flightTicketResponseEntity.getStatusCode());
    }

    @Test
    public void when_getTicketItinerary_has_unknownError_then_response_internal_server_error() {
        doThrow(RuntimeException.class).when(flightTicketBO).getTicketItineraryID(anyLong());

        ResponseEntity<Object> flightTicketResponseEntity = manageFlightTicketService.getFlightTicket(TICKET_ITINERARY_ID);

        assertEquals(INTERNAL_SERVER_ERROR, flightTicketResponseEntity.getStatusCode());
    }

    private FlightTicketRequest createFlightTicketRequest() {
        flightTicketRequest.setDepartureDate("20/10/21");
        flightTicketRequest.setArrivalDate("20/10/21");
        flightTicketRequest.setCityOfOrigin("Miami");
        flightTicketRequest.setDestinationCity("Mexico");
        flightTicketRequest.setDepartureTime("10:50");
        flightTicketRequest.setArrivalTime("20:50");

        return flightTicketRequest;
    }

    private FlightTicket createFlightTicket() {
        flightTicket.setDepartureDate(of(20, 10, 25));
        flightTicket.setArrivalDate(of(20, 10, 25));
        flightTicket.setCityOfOrigin("Miami");
        flightTicket.setDestinationCity("Mexico");
        flightTicket.setPassengerName("Juan");
        flightTicket.setPassengerAge(4);
        flightTicket.setPrice(500.50);
        flightTicket.setDepartureTime(LocalTime.of(10, 50));
        flightTicket.setArrivalTime(LocalTime.of(22, 50));

        return flightTicket;
    }
}